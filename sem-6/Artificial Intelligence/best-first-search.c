#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define VERTICES 6

typedef struct Queue {
    int* items;
    int front;
    int rear;
    int capacity;
} Queue;

Queue* createQueue(int capacity) {
    Queue* queue = (Queue*)malloc(sizeof(Queue));
    queue->capacity = capacity;
    queue->front = -1;
    queue->rear = -1;
    queue->items = (int*)malloc(queue->capacity * sizeof(int));
    return queue;
}

bool isEmpty(Queue* queue) {
    return queue->rear == -1;
}

void enqueue(Queue* queue, int item) {
    if (queue->rear == queue->capacity - 1) {
        printf("Queue Overflow\n");
        return;
    }
    if (isEmpty(queue)) {
        queue->front = 0;
    }
    queue->rear++;
    queue->items[queue->rear] = item;
}

int dequeue(Queue* queue) {
    if (isEmpty(queue)) {
        printf("Queue Underflow\n");
        return -1; // Or some other error value
    }
    int item = queue->items[queue->front];
    if (queue->front == queue->rear) {
        queue->front = -1;
        queue->rear = -1;
    } else {
        queue->front++;
    }
    return item;
}

void bestFirstSearch(int adjMatrix[][VERTICES], int startNode) {
    bool* visited = (bool*)calloc(VERTICES, sizeof(bool)); // Initialize visited array
    Queue* queue = createQueue(VERTICES);

    visited[startNode] = true;
    enqueue(queue, startNode);

    while (!isEmpty(queue)) {
        int currentNode = dequeue(queue);
        printf("%d ", currentNode);

        for (int neighbor = 0; neighbor < VERTICES; neighbor++) {
            if (adjMatrix[currentNode][neighbor] && !visited[neighbor]) {
                visited[neighbor] = true;
                enqueue(queue, neighbor);
            }
        }
    }

    free(visited);
    free(queue->items);
    free(queue);
}

enum {
    S,
    A,
    B,
    C,
    D,
    E
} NODE;

void test() {

    // Create an adjacency matrix (example graph)
    int adjMatrix[VERTICES][VERTICES] = {0} ;

    // Add edges to the adjacency matrix
    adjMatrix[S][A] = 1;
    adjMatrix[S][B] = 1;
    adjMatrix[A][D] = 1;
    adjMatrix[B][E] = 1;
    adjMatrix[C][E] = 1;
    adjMatrix[D][E] = 1;

    printf("Best-First Search starting from node %d : ", S);
    bestFirstSearch(&adjMatrix[0], 0);
    printf("\n");
}

int main(void) { test(); }
