function setDate(){
    const departure_date = document.getElementById('departure_date')
    let date = new Date()
    let parsed_date = date.toJSON()
    departure_date.min = parsed_date.split("T")[0]
    departure_date.value = parsed_date.split("T")[0]
}
setDate()

document.querySelector("form").addEventListener( "submit", function (e) {
    const from_station = document.getElementById("from_station")
    const to_station = document.getElementById("to_station")
    e.preventDefault()
    if (from_station.value == to_station.value) {
        alert("Both stations should be different")
    } else {
        alert("Submission Successful!")
        setDate()
    }
})