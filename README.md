# B.Sc [Comp Hons.] Degree Course Practical Program Codes

## SEM-I
1. Introduction to C/C++ programming language
1. Understanding of basic programming concepts and language syntax
1. Fundumentals of different programming paradigm (Procedural, Functional, OOP)

## SEM-II
1. Introduction to Java programming language
1. Building software with pure object-oriented model
1. Elaborated discussios over exclusive topics
    - Multi-threading
    - Exception Handling
    - Java Applets (Depricated Feature)

## SEM-III
1. Designing and studying various data-structures models (using multiple
   paradigms)
1. Solving real-world problems under the abstraction of data structures
1. Operating system creation based unitary modules and theory (in C lang)
1. Designing Scheduling algorithms

## SEM-IV
1. Analysis of programming algorithms (complexity based)
1. Probing Graph-based complex algorithms with higher computational dependencies and
   optimization needs
1. Interaction with Data Manipulation and Storage System (MySQL)

## SEM-V
1. 8085 μP programming
  - Architecture
  - Instruction Sets
  - Functionality and Limitations
1. HTML-CSS-JavaScript programming
1. Introduction to Java Servlet Pages (JSP) 
1. Data Mining (Classification and Clustering)

## SEM-VI
1. Artificial Intelligence
1. Computer Graphics
1. Big Data
