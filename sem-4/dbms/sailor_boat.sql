--CREATE DB
CREATE DATABASE IF NOT EXISTS Sailor_db;
use Sailor_db

-- CREATE SCHEMA
CREATE TABLE IF NOT EXISTS Sailors(
    sid INT PRIMARY KEY,
    s_name VARCHAR(50),
    rating INT,
    age INT,
    CONSTRAINT chk_sid
    CHECK (sid >= 100 AND sid <= 10000),
    CONSTRAINT chk_rating
    CHECK (rating >= 1 AND rating <= 10),
    CONSTRAINT chk_age
    CHECK (age >= 18 AND age <= 60)
);

CREATE TABLE IF NOT EXISTS Boats(
    bid INT PRIMARY KEY,
    b_name VARCHAR(50),
    color CHAR(10),
    CONSTRAINT chk_bid
    CHECK (bid >= 100 AND bid <= 300),
    CONSTRAINT chk_color
    CHECK (color IN ('RED', 'GREEN', 'YELLOW', 'BLUE'))
);

CREATE TABLE IF NOT EXISTS Reserves(
    sid INT,
    bid INT,
    day DATE,
    CONSTRAINT fk_sailors_sid
    FOREIGN KEY (sid) REFERENCES Sailors(sid),
    CONSTRAINT fk_boats_bid
    FOREIGN KEY (bid) REFERENCES Boats(bid)
);

-- INSERT DATA
INSERT IGNORE INTO Sailors VALUES
    (102, 'Naomi James', 8, 29),
    (105, 'Captain Ahab', 9, 34),
    (108, 'Ellen MacArthur', 7, 25),
    (111, 'Peter Blake', 6, 27),
    (114, 'Popeye', 9, 35),
    (117, 'Thomas Berwick', 5, 24),
    (120, 'Sinbad', 8, 39),
    (123, 'James Cook', 6, 37),
    (126, 'David Hackworth', 4, 23),
    (129, 'Jack Sparrow', 9, 31),
    (132, 'William Kid', 6, 24),
    (135, 'Robinson Crusoe', 8, 30);

INSERT IGNORE INTO Boats VALUES
    (100, 'Hinckley Picnic Boat', 'BLUE'),
    (103, 'Boston Whaler Outrage', 'RED'),
    (180, 'Emerald Coast 32', 'GREEN'),
    (215, 'Scout 215 XSF', 'YELLOW'),
    (225, 'Yamaha AR210', 'RED'),
    (245, 'Sunseeker Predator', 'BLUE'),
    (280, 'Jeanneau Merry Fisher', 'BLUE'),
    (285, 'Yellowfin 26 Hybrid', 'YELLOW');

INSERT IGNORE INTO Reserves VALUES
    (102, 180, '2023-03-01'),
    (108, 245, '2023-03-02'),
    (126, 280, '2023-03-03'),
    (132, 100, '2023-03-04'),
    (105, 285, '2023-03-05'),
    (135, 280, '2023-03-06'),
    (129, 100, '2023-03-06'),
    (135, 103, '2023-03-06'),
    (111, 245, '2023-03-07'),
    (126, 100, '2023-03-08'),
    (120, 215, '2023-03-09'),
    (102, 285, '2023-03-10'),
    (111, 280, '2023-03-11'),
    (132, 180, '2023-03-12'),
    (117, 103, '2023-03-13'),
    (102, 245, '2023-03-14'),
    (108, 245, '2023-03-15'),
    (132, 225, '2023-03-16'),
    (120, 180, '2023-03-17'),
    (135, 103, '2023-03-18'),
    (100, 100, '2023-03-19'),
    (132, 285, '2023-03-20');

-- QUERY-1
SELECT S1.rating, AVG(age) AS avg_age
FROM Sailors S1
GROUP BY S1.rating
HAVING 1 < (SELECT COUNT(*) 
            FROM Sailors S2 
            WHERE S1.rating = S2.rating);

-- QUERY-2
SELECT DISTINCT S1.sid
FROM Sailors S1
JOIN Reserves R1 ON S1.sid = R1.sid
JOIN Boats B1 ON R1.bid = B1.bid
WHERE B1.color = 'RED' AND S1.sid NOT IN (
    SELECT S2.sid
    FROM Sailors S2
    JOIN Reserves R2 ON S2.sid = R2.sid
    JOIN Boats B2 ON R2.bid = B2.bid
    WHERE B2.color = 'GREEN');

-- QUERY-3
SELECT DISTINCT S.s_name
FROM Sailors S
JOIN Reserves R ON S.sid = R.sid
WHERE R.bid = 103;

-- DROP DB
DROP DATABASE IF EXISTS Sailor_db;
