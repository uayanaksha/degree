-- CREATE DB
CREATE DATABASE IF NOT EXISTS Bank_db;
use Bank_db

-- CREATE SCHEMA
CREATE TABLE IF NOT EXISTS Branch (
    branch_name VARCHAR(50) PRIMARY KEY,    
    branch_city VARCHAR(50),
    assets      INT,
    CONSTRAINT chk_asset_validity
    CHECK (assets > 0)
);

CREATE TABLE IF NOT EXISTS Account (
    account_no INT PRIMARY KEY CHECK(account_no > 400000000 AND account_no < 900000000),
    branch_name VARCHAR(50),
    balance INT,
    FOREIGN KEY(branch_name) REFERENCES Branch(branch_name)
);

CREATE TABLE IF NOT EXISTS Depositor (
    customer_name VARCHAR(50),
    account_no INT,
    FOREIGN KEY(account_no) REFERENCES Account(account_no)
);

CREATE TABLE IF NOT EXISTS Customer (
    customer_name VARCHAR(50) PRIMARY KEY,
    street VARCHAR(50),
    city VARCHAR(50)
);

CREATE TABLE IF NOT EXISTS Loan (
    loan_no INT PRIMARY KEY,
    branch_name VARCHAR(50) NOT NULL,
    amount INT,
    CONSTRAINT chk_amount_validity
    CHECK (amount > 0)
);

CREATE TABLE IF NOT EXISTS Borrower (
    customer_name VARCHAR(50),
    loan_no INT
);

-- INSERT DATA
INSERT IGNORE INTO Branch (branch_city, branch_name, assets) VALUES
    ('Durgapur', 'AXIS Durgapur Branch', 300000000),
    ('Durgapur', 'ICICI Durgapur Branch', 12000000),
    ('Durgapur', 'PNB Durgapur Branch', 1500000000),
    ('Durgapur', 'SBI Durgapur Branch', 2500000000),
    ('Haldia', 'Kotak Haldia Branch', 20000000000),
    ('Kolkata', 'HSBC Shyambazar Branch', 20000000),
    ('Kolkata', 'RBL Central Branch', 20000000000),
    ('Siliguri', 'HDFC Siliguri Branch', 300000000);

INSERT IGNORE INTO Account VALUES
    (555333901, 'AXIS Durgapur Branch',     11400),
    (555333902, 'ICICI Durgapur Branch',    15300),
    (555333903, 'PNB Durgapur Branch',      13200),
    (555333904, 'SBI Durgapur Branch',      11100),
    (555333905, 'AXIS Durgapur Branch',     10000),
    (555333906, 'ICICI Durgapur Branch',    8890),
    (555333907, 'PNB Durgapur Branch',      7780),
    (555333908, 'Kotak Haldia Branch',      6670),
    (555333909, 'Kotak Haldia Branch',      5560),
    (555333910, 'HSBC Shyambazar Branch',   4450),
    (555333911, 'HSBC Shyambazar Branch',   4440),
    (555333912, 'HSBC Shyambazar Branch',   17430),
    (555333913, 'HSBC Shyambazar Branch',   10420),
    (555333914, 'HDFC Siliguri Branch',     11410),
    (555333915, 'HDFC Siliguri Branch',     10500);

INSERT IGNORE INTO Depositor VALUES
     ('Adrija Bal', 555333901),
     ('Himadri Churiwala', 555333902),
     ('Rabin Kumar', 555333903),
     ('Sameer Patil', 555333904),
     ('Soham Das', 555333905),
     ('Tanisha Patil', 555333906),
     ('Tapas Biswas', 555333907),
     ('Saheli Karmakar', 555333908),
     ('Sneha Chaudhury', 555333909),
     ('Dinabandhu Seth', 555333910),
     ('Ganesh Paul', 555333911),
     ('Jason John', 555333912),
     ('Lallan Jadav', 555333913),
     ('Anima Das', 555333914),
     ('Sikat Mondal', 555333915);

-- QUERY-1
SELECT DISTINCT D.customer_name FROM Depositor D
JOIN Account A ON D.account_no = A.account_no
JOIN Branch BR ON BR.branch_name = A.branch_name
WHERE BR.branch_city = 'Durgapur';

-- QUERY-2
SELECT B.branch_name , AVG(A.balance) as avg_bal
FROM Account A 
JOIN Branch B 
ON A.branch_name = B.branch_name
GROUP BY B.branch_name 
HAVING AVG(A.balance) > 10000;

-- DROP DB
DROP DATABASE IF EXISTS Bank_db;
