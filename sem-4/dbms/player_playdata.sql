-- CREATE DB
CREATE DATABASE IF NOT EXISTS Cricket_db;
use Cricket_db

-- CREATE SCHEME
CREATE TABLE IF NOT EXISTS Player (
    name VARCHAR(50),
    country CHAR(3),
    age INT CHECK (age > 0 AND age <= 40)
);

CREATE TABLE IF NOT EXISTS Playdata (
    name_of_player VARCHAR(50),
    match_date DATE,
    runs INT CHECK (runs >= 0 AND runs <= 500),
    wickets INT CHECK (wickets >= 0 AND wickets <= 10),
    catches INT CHECK (catches >= 0 AND catches <= 10),
    against CHAR(3)
);

-- INSERT DATA
INSERT IGNORE INTO Player VALUES 
    ('Virat Kohli', 'IND', 35),
    ('David Warner', 'AUS', 37),
    ('Soumya Sarkar', 'BAN', 35),
    ('Dawid Malan', 'ENG', 36),
    ('Kane Williamson', 'NZ', 33),
    ('Babar Azam', 'PAK', 29);

INSERT IGNORE INTO Playdata (match_date, against, name_of_player, runs, catches, wickets) VALUES 
    ('2024-01-01', 'AUS', 'Virat Kohli', 63, 2, 2),
    ('2024-01-01', 'IND', 'David Warner', 47, 0, 0),
    ('2024-01-02', 'BAN', 'Virat Kohli', 43, 3, 3),
    ('2024-01-02', 'IND', 'Soumya Sarkar', 72, 1, 1),
    ('2024-01-03', 'ENG', 'Virat Kohli', 75, 0, 0),
    ('2024-01-03', 'IND', 'Dawid Malan', 19, 1, 1),
    ('2024-01-04', 'IND', 'Kane Williamson', 83, 2, 2),
    ('2024-01-04', 'NZ', 'Virat Kohli', 21, 3, 3),
    ('2024-01-05', 'IND', 'Babar Azam', 7, 0, 0),
    ('2024-01-05', 'PAK', 'Virat Kohli', 92, 3, 0),
    ('2024-01-06', 'AUS', 'Soumya Sarkar', 67, 2, 2),
    ('2024-01-06', 'BAN', 'David Warner', 63, 1, 1),
    ('2024-01-07', 'AUS', 'Dawid Malan', 36, 2, 2),
    ('2024-01-07', 'ENG', 'David Warner', 71, 2, 2),
    ('2024-01-08', 'AUS', 'Kane Williamson', 45, 1, 1),
    ('2024-01-08', 'NZ', 'David Warner', 22, 0, 0),
    ('2024-01-09', 'AUS', 'Babar Azam', 49, 0, 0),
    ('2024-01-09', 'PAK', 'David Warner', 84, 3, 3);

-- QUERY-1
SELECT DISTINCT PD.match_date, P1.country AS team1, P.country AS team2  FROM Playdata PD
JOIN Player P ON  PD.against = P.country
JOIN Player P1 ON P1.name = PD.name_of_player
GROUP BY PD.match_date;

-- DROP DB
DROP DATABASE IF EXISTS Cricket_db;
