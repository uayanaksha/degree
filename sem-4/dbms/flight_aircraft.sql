--CREATE DB
CREATE DATABASE IF NOT EXISTS Flight_db;
use Flight_db

-- CREATE SCHEMA
CREATE TABLE IF NOT EXISTS Flights ( 
    Flno CHAR(4),
    From_ VARCHAR(50),
    To_ VARCHAR(50),
    Distance_in_kms INT,
    Departs DATE,
    Arrives DATE,
    CONSTRAINT check_distance
    CHECK(Distance_in_kms>=300 AND Distance_in_kms<=100000),
    CONSTRAINT check_arrival_and_departure
    CHECK(Arrives>=Departs)
);

CREATE TABLE IF NOT EXISTS Aircraft ( 
    Aid CHAR(5) PRIMARY KEY,
    Aname VARCHAR(50),
    Cruise_range_in_nm INT
);

CREATE TABLE IF NOT EXISTS Employee (
    Eid CHAR(4) PRIMARY KEY,
    Ename VARCHAR(50),
    Salary INT CHECK(Salary >= 500000 AND Salary <= 3000000)
);

CREATE TABLE IF NOT EXISTS Certified (
    Eid CHAR(4),
    Aid CHAR(5),
    CONSTRAINT fk_employee_eid
    FOREIGN KEY (Eid) REFERENCES Employee(Eid),
    CONSTRAINT fk_aircraft_aid
    FOREIGN KEY (Aid) REFERENCES Aircraft(Aid)
);

-- INSERT DATA
INSERT IGNORE INTO Aircraft VALUES
    ('BXG77', 'BOEING 777', 8555),
    ('BXG37', 'BOEING 737', 3800),
    ('ABS30', 'Airbus A330', 6350),
    ('COC19', 'Comac C919', 2200),
    ('IIL96', 'Ilyushin Il-96', 6200),
    ('SSJ10', 'Sukhoi Superjet 100', 4620);

INSERT IGNORE INTO Employee VALUES
    ('GT05', 'Gaurav Taneja',   900000),
    ('JB15', 'Jimmy Buffett',   1200000),
    ('BP55', 'Brad Pitt',       1500000),
    ('TC70', 'Tom Cruise',      2500000),
    ('MF30', 'Morgan Freeman',  1300000);

INSERT IGNORE INTO Certified VALUES
    ('MF30', 'COC19'),
    ('BP55', 'BXG37'),
    ('GT05', 'ABS30'),
    ('TC70', 'SSJ10'),
    ('JB15', 'BXG77');

-- Query-1
SELECT SUM(Salary) 
FROM Employee;

-- Query-2
SELECT DISTINCT Eid 
FROM Certified 
INNER JOIN Aircraft 
ON Certified.Aid = Aircraft.Aid 
WHERE Aircraft.Aname LIKE 'Boeing%';

-- Query-3
SELECT * 
FROM Employee
ORDER BY Salary DESC
LIMIT 1
OFFSET 1;

-- DROP DB
DROP DATABASE IF EXISTS Flight_db;
