-- CREATE DB
CREATE DATABASE IF NOT EXISTS Company_db;
use Company_db

-- CREATE SCHEMA
CREATE TABLE IF NOT EXISTS Employee(
    emp_name VARCHAR(50) PRIMARY KEY,
    street VARCHAR(50),
    city VARCHAR(50)
);

CREATE TABLE IF NOT EXISTS Works(
    emp_name VARCHAR(50),
    company_name VARCHAR(50),
    salary INT DEFAULT 10000
);

CREATE TABLE IF NOT EXISTS Company(
    company_name VARCHAR(50) PRIMARY KEY,
    city VARCHAR(50),
    CONSTRAINT chk_company_name
    CHECK (company_name IN ('IOC', 'ABC', 'XYZ'))
);

CREATE TABLE IF NOT EXISTS Manages(
    emp_name VARCHAR(50),
    manager_name VARCHAR(50),
    CONSTRAINT fk_emp_name_manages
    FOREIGN KEY (emp_name) REFERENCES Employee(emp_name)
);

-- INSERT DATA
INSERT IGNORE INTO Employee VALUES
    ('Arun Verma', 'Lajpat Nagar', 'New Delhi'),
    ('Dinesh Patel', 'Athwa Gate', 'Surat'),
    ('Dipender Chaudhury', NULL, 'New Delhi'),
    ('Harpreet Kaur', 'Lodhi Colony', 'New Delhi'),
    ('Tapan Das', NULL, 'Surat'),
    ('Tarun Goyel', 'Ghod Dod Road', 'Surat'),
    ('Varun Sharma', 'Adajan Road', 'Surat');

INSERT IGNORE INTO Company VALUES
    ('ABC', 'New Delhi'),
    ('IOC', 'Surat'),
    ('LMN', 'Lucknow'),
    ('XYZ', 'New Delhi');

INSERT IGNORE INTO Works VALUES
    ('Arun Verma', 'ABC', 25000),
    ('Dinesh Patel', 'IOC', 16800),
    ('Dipender Chaudhury', 'ABC', 20000),
    ('Harpreet Kaur', 'ABC', 22500),
    ('Tapan Das', 'IOC', 15500),
    ('Tarun Goyel', 'IOC', 16000),
    ('Varun Sharma', 'IOC', 18800);

INSERT IGNORE INTO Manages VALUES
    ('Dinesh Patel', 'Varun Sharma'),
    ('Dipender Chaudhury', 'Arun Verma'),
    ('Harpreet Kaur', 'Arun Verma'),
    ('Tapan Das', 'Varun Sharma'),
    ('Tarun Goyel', 'Varun Sharma');

-- QUERY-1
SELECT * 
FROM Works 
WHERE company_name = 'IOC'
ORDER BY Salary DESC;

-- QUERY-2
SELECT DISTINCT E.emp_name, E.street, E.city
FROM Employee E
INNER JOIN Works W
ON E.emp_name = W.emp_name
WHERE W.company_name = 'ABC'
AND W.salary > 10000;


-- QUERY-3
SELECT DISTINCT emp_name 
FROM Works 
WHERE company_name != 'IOC';

-- QUERY-4
SELECT company_name 
FROM Company 
WHERE city IN ( SELECT city
                FROM Company
                WHERE company_name = 'ABC');

-- DROP DB
DROP DATABASE IF EXISTS Company_db;
