#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#ifndef STACK_H
#define STACK_H

typedef struct Stack{
    int *arr;
    int head;
    int maxVol;
} Stack;

Stack* init(unsigned size);
Stack* destroy(Stack *st);
bool isEmpty(Stack *st);
bool isFull(Stack *st);
void push(Stack *st, int val);
int pop(Stack *st);

#endif
