#include "stack.h"

Stack* init(unsigned size){
    Stack *ptr = (Stack*) malloc(sizeof(Stack));
    if(ptr){
        ptr->arr = calloc(size, sizeof(int));
        ptr->head = -1;
        ptr->maxVol = size;
    }   return ptr;
}

Stack* destroy(Stack *st){
    Stack *ptr = st;
    if(ptr){
        free(ptr->arr);
        free(ptr);
    }   return ptr;
}

bool isEmpty(Stack *st){
    return st->head == -1;
}

bool isFull(Stack *st){
    return st->maxVol == st->head + 1;
}

void push(Stack *st, int val){
    if(isFull(st)) return;
    st->head++;
    st->arr[st->head] = val;
}

int pop(Stack *st){
    int elem;
    elem = isEmpty(st) ? -1 : st->arr[st->head--];
    return elem;
}
