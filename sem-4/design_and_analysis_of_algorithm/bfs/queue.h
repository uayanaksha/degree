#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#ifndef QUEUE_H
#define QUEUE_H
typedef struct Queue{
    int *arr;
    int head, tail;
    int maxVol;
} Queue;

Queue* init(unsigned size);
void destroy(Queue *que);
bool isEmpty(Queue *que);
bool isFull(Queue *que);
void enqueue(Queue *que, int val);
int dequeue(Queue *que);

#endif
