#include "queue.h"

Queue* init(unsigned size){
    Queue *que = (Queue*) malloc(sizeof(Queue));
    if(que){
        que->arr = calloc(size, sizeof(int));
        que->head = -1;
        que->tail = -1;
    }   return que;
}

void destroy(Queue *que){
    if(!que) return;
    free(que->arr);
    free(que);
}

bool isEmpty(Queue *que){
    return que->head == -1;
}

bool isFull(Queue *que){
    return que->maxVol == que->tail - que->head + 1;
}

void enqueue(Queue *que, int val){
    if(isFull(que)) return;
    if(isEmpty(que)) que->head++;
    que->tail++;
    que->arr[que->tail] = val;
}

int dequeue(Queue *que){
    int elem = -1;
    if(!isEmpty(que)){
        elem = que->arr[que->head];
        if(que->head == que->tail){
            que->head = -1;
            que->tail = -1;
        }   else {
            que->head++;
        }
    }   return elem;
}
