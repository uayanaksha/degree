INPUT=test-sort.c
OUTPUT=test-sort.out
if [ -x $OUTPUT ] ; then
    shred -zuv $OUTPUT 2&> /dev/null
fi
cc -o $OUTPUT $INPUT
./$OUTPUT | tee output.txt
