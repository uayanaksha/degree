#ifndef SORT_T
#define SORT_T
#include "bubbleSort.c"
#include "heapSort.c"
#include "insertionSort.c"
#include "selectionSort.c"
#include "mergeSort.c"
#include "quickSort.c"
#include "countingSort.c"
#include "radixSort.c"
#endif
