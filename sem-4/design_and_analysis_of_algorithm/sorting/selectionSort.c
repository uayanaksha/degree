#include "header.h"
const char* selection_sort(int array[], int size){
    int minIdx;
    for(int i = 0; i < size - 1; i++){
        comparisons++;
        minIdx = i;
        for(int j = i; j < size; j++){
            comparisons++;
            if(array[j] < array[minIdx]) minIdx = j;
        }
        if (i != minIdx) {
            array[i] ^= array[minIdx];
            array[minIdx] ^= array[i];
            array[i] ^= array[minIdx];
        }
    }
    return __FUNCTION__;
}
