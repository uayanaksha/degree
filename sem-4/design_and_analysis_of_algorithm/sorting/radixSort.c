int getMax(int arr[], unsigned size){
    int max = arr[0];
    for(int i = 1; i < size; i++){
        if(max < arr[i]) max = arr[i];
    }
    return max;
}

const char* radix_sort(int arr[], int size){
    int mx = getMax(arr, size);
    for(int exp = 1; mx/exp > 0; exp *= 10){
        counting_sort(arr, size);
    }
    return __FUNCTION__;
}
