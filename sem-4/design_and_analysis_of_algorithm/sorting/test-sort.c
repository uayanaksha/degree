#include "sort.h"
#include <stdio.h>
#define SIZE 64

// Further use of function idexing through enums
enum {
    BUBBLE,
    INSERTION,
    SELECTION,
    MERGE,
    HEAP,
    QUICK,
    COUNTING,
    RADIX,
    ALGO_COUNT,
}   sort_methods_t;

// Array display method
void display(int array[], int size){
    printf("List [%d]:", size);
    for(int i = 0; i < size; i++){
        if(i > 0){
            printf(array[i-1] <= array[i] ? " <=" : " >");
        }
        printf(" %d", array[i]);
    }   printf("\n");
}

// Driver code
int main(void){
    srand(time(0));
    int array[] = {[ 0 ... SIZE ] = 0},
        duplicate[] = {[ 0 ... SIZE ] = 0};
    for(int i = 0; i < SIZE; i++){
        array[i] = rand() % 200;
        duplicate[i] = array[i];
    }   // add random integers
    const char* (*sort_algo[])(int[], int) = {
        [BUBBLE] = bubble_sort,
        [INSERTION] = insertion_sort,
        [SELECTION] = selection_sort,
        [MERGE] = merge_sort,
        [HEAP] = heap_sort,
        [QUICK] = quick_sort,
        [COUNTING] = counting_sort,
        [RADIX] = radix_sort
    };  // Sorting methods
    display(array, SIZE);   // Display OG array
    for(int i = 0; i < ALGO_COUNT; i++){
        printf("%s\n", "---");
        printf("[%s] COMPLETED!\n", sort_algo[i](array, SIZE));
        printf("Total comparisons : %d\n", comparisons);
        
        for(int i = 0; i < SIZE; i++){
            array[i] = duplicate[i];
        }   // Returning to OG state
        comparisons = 0;
    }   // Performing all sorting algorithms
    return 0;
}
