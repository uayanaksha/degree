#include "header.h"
const char* insertion_sort(int array[], int size){
    int i = 1,
        j = 0,
        key = 0;
    while(i < size){
        comparisons++;
        key = array[i];
        j = i - 1;
        while(j >= 0 && array[j] > key){
            comparisons++;
            array[j+1] = array[j];
            --j;
        }   array[j+1] = key;
        ++i;
    }
    return __FUNCTION__;
} 
