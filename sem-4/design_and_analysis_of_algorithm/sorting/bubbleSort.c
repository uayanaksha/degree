#include "header.h"
const char* bubble_sort(int array[], int size){
    for(int i = 0; i < size; i++){
        comparisons++;
        for(int j = i; j < size; j++){
            comparisons++;
            if(array[i]<=array[j]) continue;
            array[i] ^= array[j];
            array[j] ^= array[i];
            array[i] ^= array[j];
        }
    }   return __FUNCTION__;
}
