#include "prims/prims.h"

int main(void){
    unsigned vCount, eCount;
    printf("Enter number of vertices: ");
    scanf("%u", &vCount);
    printf("Enter number of edges: ");
    scanf("%u", &eCount);
    if(2*eCount > vCount*(vCount-1)) return 1;
    Graph graph = INITGraph(vCount);
    for (int i = 0, i1, i2, weight; i < eCount; ) {
        printf("Enter initial idx: ");
        scanf("%d", &i1);
        printf("Enter final idx: ");
        scanf("%d", &i2);
        printf("Enter weight: ");
        scanf("%d", &weight);
        if(i1 == i2){
            printf("Self-loop not allowed!\n");
        } else if (i1 >= vCount || i1 < 0|| i2 >= vCount || i2 < 0) {
            printf("Index out of bound!\n");
        } else if (weight < 0) {
            printf("Negative weight not allowed!\n");
        } else {
            graph[i1][i2] = weight;
            graph[i2][i1] = weight;
            i++;
        }
    }
    display(graph, vCount);
    primMST(graph, vCount);
    DELETEGraph(graph, vCount);
    return 0;
}
