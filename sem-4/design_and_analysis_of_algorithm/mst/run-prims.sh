#!/bin/sh 

# creating object files
find ./prims/*.c -exec cc -c {} +

# create lib dir
if [ ! -x ./lib/ ]; then
    mkdir lib
fi

# compile object files -> library archive
find ./*.o -exec ar rvs ./lib/prims.a {} +

# remote object files
find ./*.o -exec shred -zu {} +

# compile output
if [ -a ./test-prims.c ] || [ -a ./lib/prims.a ]; then
    cc -o ./test-prims.out ./test-prims.c ./lib/prims.a
fi
