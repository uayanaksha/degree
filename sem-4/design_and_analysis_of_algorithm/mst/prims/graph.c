#include "graph.h"
Graph INITGraph(unsigned vCount){
    Graph graph = (int**)calloc(vCount, sizeof(int*));
    for(int i = 0 ; i < vCount; i++){
        graph[i] = (int*)calloc(vCount, sizeof(int));
    }   return graph;
}

void DELETEGraph(Graph graph, unsigned vCount){
    for(int i = 0; i < vCount; i++){
        free(graph[i]);
    }
    if(graph) free(graph);
}

void display(Graph graph, unsigned vCount){
    printf("List[%d][%d]:\n", vCount, vCount);
    for(int i = 0; i < vCount; i++){
        for(int j = 0; j < vCount; j++){
            printf(" %d", graph[i][j]);
        }   printf("\n");
    }   printf("\n");
}

