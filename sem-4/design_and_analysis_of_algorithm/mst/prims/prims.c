#include "prims.h"

static int minKey(int key[], bool mstSet[], unsigned vCount){
    int min = INT_MAX, min_index;
    for (int v = 0; v < vCount; v++)
        if (mstSet[v] == false && key[v] < min)
            min = key[v], min_index = v;
    return min_index;
}

static void printMST(int parent[], Graph graph, unsigned vCount){
    printf("%2s|%2s\n", "Edge", "Weight");
    for (int i = 1; i < vCount; i++){
        printf(" %.2d-%.2d ", parent[i], i);
        printf(" %d\n", graph[i][parent[i]]);
    }
}

void primMST(Graph graph, unsigned vCount){
    int parent[vCount];
    int key[vCount];
    bool mstSet[vCount];
    for (int i = 0; i < vCount; i++)
        key[i] = INT_MAX, mstSet[i] = false;
    key[0] = 0;
    parent[0] = -1;
    for (int count = 0; count < vCount - 1; count++) {
        int u = minKey(key, mstSet, vCount);
        mstSet[u] = true;
        for (int v = 0; v < vCount; v++)
            if (graph[u][v] && mstSet[v] == false
                && graph[u][v] < key[v])
                parent[v] = u, key[v] = graph[u][v];
    }
    printMST(parent, graph, vCount);
}
