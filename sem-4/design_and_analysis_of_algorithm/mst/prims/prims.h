#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <limits.h>
#include "graph.h"

#ifndef PRIMS_H
#define PRIMS_H

void primMST(Graph graph, unsigned vCount);

#endif
