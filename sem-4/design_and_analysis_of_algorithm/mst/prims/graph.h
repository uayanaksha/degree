#include <stdio.h>
#include <stdlib.h>

#ifndef GRAPH_H
#define GRAPH_H

typedef int** Graph;
Graph INITGraph(unsigned vCount);
void DELETEGraph(Graph graph, unsigned vCount);
void display(Graph graph, unsigned vCount);

#endif
